FROM haproxy:1.5

RUN useradd haproxy
RUN mkdir -p /subdue/chroot/haproxy
RUN chown haproxy:haproxy /subdue/chroot/haproxy

COPY haproxy.cfg /usr/local/etc/haproxy/haproxy.cfg
