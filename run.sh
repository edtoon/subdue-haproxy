#!/bin/bash

set -e

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
SRC_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

PROJECT_NAME="${SRC_DIR##*/}"
SYSTEM_DIR=`dirname ${SRC_DIR}`

if [ "src" = "`basename ${SYSTEM_DIR}`" ];
then
  SYSTEM_DIR=`readlink -f "${SYSTEM_DIR}/../"`
fi

CONF_DIR="${SYSTEM_DIR}/conf/${PROJECT_NAME}"

docker run -d -p 80:80 -p 443:443 \
  -v "${CONF_DIR}/server.pem:/subdue/server.pem" \
  "${PROJECT_NAME}:latest"
