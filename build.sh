#!/bin/bash

set -e

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
SRC_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

PROJECT_NAME="${SRC_DIR##*/}"
SYSTEM_DIR=`dirname ${SRC_DIR}`

if [ "src" = "`basename ${SYSTEM_DIR}`" ];
then
  SYSTEM_DIR=`readlink -f "${SYSTEM_DIR}/../"`
fi

CONF_DIR="${SYSTEM_DIR}/conf/${PROJECT_NAME}"

mkdir -p "${CONF_DIR}"

if [ ! -e "${CONF_DIR}/server.pem" ];
then
  if [ -e "/etc/ssl/openssl.cnf" ];
  then
    OPENSSL_CONF="/etc/ssl/openssl.cnf"
  elif [ -e "/usr/lib/ssl/openssl.cnf" ];
  then
    OPENSSL_CONF="/usr/lib/ssl/openssl.cnf"
  elif [ -e "/usr/local/etc/ssl/openssl.cnf" ];
  then
    OPENSSL_CONF="/usr/local/etc/ssl/openssl.cnf"
  else
    >&2 echo "Can't find openssl.cnf"
    exit 1
  fi

  openssl req -config "${SRC_DIR}/cert.cfg" -new -x509 -nodes -days 3650 -out "${CONF_DIR}/server.crt" -keyout "${CONF_DIR}/server.key"
  cat "${CONF_DIR}/server.crt" "${CONF_DIR}/server.key" > "${CONF_DIR}/server.pem"
fi

docker build -t ${PROJECT_NAME}:latest .
