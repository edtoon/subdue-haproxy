#!/bin/bash

set -e

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
SRC_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

PROJECT_NAME="${SRC_DIR##*/}"

docker logs -f `docker ps | grep "${PROJECT_NAME}:latest" | head | awk '{print $1}'`
